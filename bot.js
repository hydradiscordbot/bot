const { log } = require('console');
const { AkairoClient , CommandHandler, ListenerHandler } = require('discord-akairo');
const { MessageEmbed } = require('discord.js');

const { config } = require('dotenv');
const { join } = require('path');

config();
const { DISCORD_BOT_TOKEN, OWNERS } = process.env;
const commandsPath = join(__dirname, '..', 'commands/');
const listenersPath = join(__dirname, '..', 'listeners/');

class MyClient extends AkairoClient {
    constructor() {
        super({
            ownerID: '282777226032971786',
        }, {
            disableMentions: 'everyone'
        });
        this.commandHandler = new CommandHandler(this, {
            prefix: ['!'],
            blockBots: true,
            blockClient: true,
            allowMention: true,
            defaultCooldown: 10,
            commandUtil: true,
            directory: './commands/'
        });
        this.listenerHandler = new ListenerHandler(this, {
            directory: './listeners/'
        });
        this.commandHandler.useListenerHandler(this.listenerHandler);
        this.listenerHandler.loadAll();
        this.commandHandler.loadAll();
    }
}


const client = new MyClient();
client.login(DISCORD_BOT_TOKEN);