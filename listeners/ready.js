const { Listener } = require("discord-akairo")

class Ready extends Listener {
    constructor() {
        super('ready', {
            event: 'ready',
            emitter: 'client'
        });
    }

    exec() {

        let i = 0;
        setInterval(() => this.client.user.setActivity(`Hydra Alliance`, { type: 'WATCHING' }), 15000);
        const Guildsname = this.client.guilds.cache.map(guild => guild.name);
        const Guildsid = this.client.guilds.cache.map(guild => guild.id);
        console.log(`접속한 서버 리스트`);
        console.log(Guildsname);
        console.log(Guildsid);
        console.log(`${this.client.user.tag} is online!`);

    }
}

module.exports = Ready;