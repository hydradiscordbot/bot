const { Command } = require('discord-akairo');
const { MessageEmbed } = require('discord.js');

class AddLocation extends Command{
    constructor() {
        super('add', {
            aliases: ['a', 'ad' , 'add'],
            category: 'Battle info',
            ownerOnly: false,
            typing: true,
            quoted: false,
            args: [
                {
                    id: 'loc',
                    match: 'content'
                }
            ]
        });
    }
    async exec(message, { loc }) {
        message.channel.send(`작동 태스트중 입력값 ` + loc);
    }
}


module.exports = AddLocation;