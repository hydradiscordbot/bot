const { Command } = require('discord-akairo');
const { MessageEmbed } = require('discord.js');

class removeLocation extends Command{
    constructor() {
        super('remove', {
            aliases: ['r', 're', 'remove'],
            category: 'Battle info',
            ownerOnly: false,
            typing: true,
            quoted: false,
            args: [
                {
                    id: 'loc',
                    match: 'content'
                }
            ]
        });
    }
    async exec(message, { loc }) {
        message.channel.send(`작동 태스트중 입력값 ` + loc);
    }
}
module.exports = removeLocation;